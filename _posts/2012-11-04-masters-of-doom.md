---
title: Masters of Doom
layout: post
header: Masters of Doom
---
Skończyłem niedawno Masters of Doom [1] David Kushnera [2].

Jako programista: szkoda, że Kushner nie wdaje się w techniczne szczegóły niektórych rzeczy, ale z drugiej strony to zrozumiałe.

Jako gracz: widać, że Kushner *rozumie* o czym pisze. 

Napisana naprawdę fajnym językiem, widać ogrom twardej, żmudnej dziennikarskiej roboty która została odwalona w czasie setek wywiadów. Widać też że autor nie bał się zadawać momentami niezręcznych czy niewygodnych pytań i traktował "Two Johns" jak ludzi, nie jak żyjące pomniki (czego nie mogę niestety powiedzieć o najnowszej książce Isaascona...).
Kushner, za co należą mu się brawa, stara się postawić się w pozycji osoby stojącej z zewnątrz, bez przyznawania w konfliktowych sytuacjach, żadnej ze stron racji. Co prawie zawsze mu wychodzi, chociaż pod koniec książki nie sposób odnieść wrażenia że jednak kibicuje trochę bardziej Carmackowi.

Szkoda też że książka kończy się w takim momencie, w jakim się kończy - nie opowiada o mini-development hell Dooma 3, nie ma też miejsca w którym można by opisać co tym razem odkrył Carmack w id Tech 4, dlaczego nikt nie licencjonował tego silnika bo wszyscy używali UE2...

Przez to, że ta książka ma już blisko 10 lat naprawdę brakuje opowiedzenia "historii najnowszej", ale z drugiej strony ciężko to uważać za argument przeciwko niej.

W każdym razie: serdecznie polecam, 8/10.

P.S. Jestem na początku "Jacked: The Outlaw Story of Grand Theft Auto" tego samego autora, więc możecie się spodziewać drugiej minirecenzji.

P.S. 2 Nie wiem czemu, ale bardzo mi się spodobało że w wydaniu które kupiłem, znalazła się [ta strona](http://distilleryimage4.s3.amazonaws.com/abfb8466269f11e28c261231381058f1_7.jpg):

Sam font fajny, tylko czytanie fragmentów w kursywie dłuższych niż pół linijki było piekłem.

[1]: http://en.wikipedia.org/wiki/Masters_of_Doom
[2]: http://en.wikipedia.org/wiki/David_Kushner
