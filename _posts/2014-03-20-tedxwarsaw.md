---
title: TEDxWarsaw
layout: post
header: TEDxWarsaw
---

Do TED, o różnorakich TEDx nawet nie wspominając, byłem nastawiony dość sceptycznie. 

"Middlebrow megachurch infotainment", jak określił to Benjamin Bratton w swoim genialnym tekście/talku [1], idealnie oddaje moje uczucia, tylko że dużo zgrabniej niż ja kiedykolwiek będę w stanie. 

TEDy mają "inspirować", pozwolić nam się dowiedzieć czegoś nowego o technologii, mają być pełne pomysłów którymi warto się podzielić. 

Zamiast tego, dostajemy bzdurne i skrajnie nudne anegdotki z życia prelegentów, doprowadzające do żenady słyszane po raz setny "wyjdź ze swojej strefy komfortu, just do it!" i klepanie się po plecach #1% (TED) i Higher Midle Class / Middle Middle Class (TEDx).

TEDy mają też podobno dużą wartość edukacyjną, szerzą łatwodostępną naukę dla mas. Biorąc pod uwagę, ile w internecie jest wideo z przemówień na tych konferencjach, jakaś część z nich taka na pewno jest. To jednak nie te są najczęściej oglądane. Dwadzieścia najpopularniejszych talków [2] doskonale oddaje prawdziwy charakter tego, o czym ludzie myślą, kiedy mówią o TEDach.

Pierwsze miejsce, wystąpienie z 2006, o tym jak szkoła zabija kreatywność. Trudno o lepszą publiczność na tego typu wystąpienia, znakomita większość MMC i wyżej doskonale się przecież identyfikuje z ideą "nudnej szkoły".

Drugie miejsce, 2008, ckliwa historyjka o udarze i nowej perspektywie na życie.

Trzecie miejsce, 2010, próba dekonstrukcji tego, co czyni liderów prawdziwymi liderami.

Dalej jest tak samo. Wymienione talki są rzecz jasna świetnie wykonane, ogląda się je z przyjemnością i tak dalej... tyle że kompletnie nic z tego nie wynika. Jasne, przez chwilę będę pod wrażeniem, poczuję się dobrze ze sobą, doznam jakiegoś ulotnego oświecenia, po czym pójdę spać i rano o wszystkim zapomnę. 

W gruncie rzeczy to nie byłby jakiś dramatyczny problem - żyjemy w końcu w 2014 roku, w telewizji mamy Warsaw Shore i Natalię Siwiec, więc brak wyniesienia czegoś wartościowego z oglądanego wideo nie jest ani niczym dziwnym, ani strasznym, ani rzadkim - gdyby nie specyficzny klimat i otoczka TEDa.

## Sekta

TED jest bardzo często (czy to przez gawiedź zwabioną przez kolorowe prezentacje, czy to przez prelegentów, czy to przez samo TED Global) mitologizowany, jako wielkie wydarzenie, gdzie wartościowe pomysły, wielkie idee i przełomowe odkrycia wylewają się brzegami. W rzeczywistości TED to wspomniany wcześniej "middlebrow megachurch infotainment" podany w pachnącej z lekka sekciarstwem otoczce. 

Sam proces aplikacji (musisz udowodnić, że jesteś godzien dołączyć do naszych legionów, śmiertelniku). 

Prośba o RSVP, z zaznaczeniem, że jeżeli nie możesz przyjść na całe 10h, to nie jesteś mile widziany, niewierny. 

Zaproszenie na warsztaty z networkingu (like, seriously, what the fuck). 

Prośba o ściągnięcie jakiejś durnej appki mającej jeszcze bardziej wspierać ten networking. 

Wykonywanie durnych gestów podczas samej konferencji.

Sala, na której odbywała się konferencja zamykana przed ludźmi kiedy jeszcze była w 1/4 wolna, odprawiając ich do overflow roomu.

Być może jestem jakimś kosmicznym buzkillem i czegoś tutaj nie rozumiem, ale wszystkie te elementy powodowały, że czułem się wybitnie niekomfortowo.

A cała ta szopka, to opakowanie dla 17 przemówień, podzielonych na cztery sesje, łącznie 10 godzin. 

Z czego warte posłuchania było naprawdę jedno, kolejne półtora było "interesujące".

## Pierwsza sesja

Yuri, który wyszedł i opowiedział trzy anegdotki ze swojego życia. Opowiedział ciekawie, świetnym angielskim... ale to ciągle było to samo i do porzygu "spełniaj swoje marzenia", "wyjdź ze strefy komfortu", "just do it", bla bla bla bla. 

Prezentacja o holografii, która raz po raz milionowy w moim życiu mówiła mi o dualiźmie korpuskularno-falowym, a raz rzucała mi w twarz fizykę na poziomie doktoratu. W zestawie nienajlepszy angielski i brak jakiegokolwiek śladu wyjaśnienia, dlaczego mam się w ogóle przejmować hologramami.

Kamila Staryga, która opowiadała o tym, jak to jej marzeniem/wizją jest to, że kiedyś na CV, oprócz polskiego i angielskiego, w znajomości języków będziemy też wpisywać programowanie i w ogóle jakie to programowanie jest ważne, bo wszędzie nas otaczają komputery. Bez wytłumaczenia, dlaczego mielibyśmy wszyscy umieć programować, oprócz tego że otaczają nas komputery. 

Prezentacja o tym, jak w Holandii ludzie są zadowoleni z życia i że to na pewno ze względu na jazdę rowerami i trzy zdjęcia na krzyż, które pokazywały jak wyglądałyby popularne ulice w Warszawie, gdybyśmy zwęzili jezdnie i na ich miejsce pojawiły się deptaki i ścieżki rowerowe. 
Tara Ross najwyraźniej nigdy nie słyszała hasełka "correlation does not imply causation", ale paskudne uproszczenie "ludzie w Holandii są zadowoleni z życia bo jeżdżą wszędzie na rowerach, wyrzućmy samochody z miast i wszyscy będą szczęśliwsi" doskonale pokazuje wszystko, co jest nie tak z TEDem.

Artystka, która nie pasowała do otoczenia, więc wyjechała z Polski i szydełkuje full-body kostiumy, bo pod włóczką nie widać koloru skóry i płci i wszyscy jesteśmy tacy sami i w ogóle super. Do tego jeszcze odniesienie do butterfly effect, bo w samolocie który lądował na rzece Hudson była jakaś executive z firmy, która daje jej włóczkę i gdyby się rozbił, to nie mogłaby kontynuować swojej sztuki i w ogóle jak to wszystko jest powiązane i nie mamy o tym nawet pojęcia.

Jan Blake, która wyszła i opowiedziała nam wymyśloną historyjkę o pasterzu wielbłądów (?), z czego wyniosłem tyle, że koszmarnie nie rozumiem co się właśnie wokół mnie dzieje i jaki to ma w ogóle związek z czymkolwiek.

##Druga sesja.

Polski ambasador Khan Academy, o tym że szkoła może być lepsza. To akurat na tyle perełka na tle tego całego narzekania na edukację, że Khan (i przez rozszerzenie Lech Mankiewicz) faktycznie mają jakieś pomysły, jak można to zrobić inaczej i wcielają je w życie, z mniejszym lub większym sukcesem. 

Jakieś bzdurne informacje o tym, "jak to jest z tymi singlami", szkoda że nieskończenie bardziej ciekawy jest dowolny wpis z bloga OKCupid [3] \(straszna szkoda, że już nikt go nie prowadzi...). Czułem się, jakbym trafił na zebranie redakcyjne Bravo, czy innego Cosmopolitana. 

Kosmiczny bełkot na tle zdjęć mody Warszawy na przestrzeni wieków, w stylu Paulo Coelho, z absolutnym brakiem jakiegokolwiek przesłania, wiadomości, wizji, nawet durnej. Wyjście na scenę i semidramatyczna melorecytacja napisanych wcześniej Coelhizmów. 

Po tym z kolei zdecydowanie najsilniejsza prezentacja całego dnia, czyli Maria Mach o "zdrowym zaniedbaniu". O tym, jak relatywnie nagle dzieci stały się centrum uwagi w rodzinie i kulturze, jak nieustannie bombardujemy je aktywnościami, prztyłaczamy. O tym że powinniśmy dzieci wspierać i pozwalać im odkrywać świat samemu, z nami w tle, a nie być krok z przodu i pokazywać wszytko palcem. Że doprowadziliśmy do tego że dzieci nie potrafią się teraz nudzić. Że uatrakcyjnianie szkoły, żeby dzieci były nią bardziej zainteresowane może mieć też ciekawy skutek uboczny - nie wszystko co robimy w "dorosłym życiu" jest ciekawe i atrakcyjne, co się stanie z takimi dziećmi w momencie zderzenia z rzeczywistością. Że nie wszystko co ważne, jest atrakcyjne (to akurat jest problem z którym mogę się nawet trochę identyfikować).

Paulina Braun, w wystąpieniu o tym, jak ignorujemy starych dramatycznym do tego stopnia, że do teraz nie jestem pewien, czy ona tak na serio się przejmuje seniorami, czy próbowała naśladować jakieś wystąpienie, które gdzieś widziała w internecie i nerwy ją zjadły i wyszła parodia. Uważam, że jej pomysł na rozwiązanie tego problemu (młodzieżowe imprezy z udziałem seniorów, aktywizowanie starszych, żeby zajęli się rzeczami typowo kojarzonymi z młodzieżą (DJ)) za wybitnie nietrafiony, ale każdemu jego porno.

##Trzecia sesja.

Tomasz Tomaszewski, który... sam nie wiem. Z jednej strony to było jedno z tych wystąpień przy których byłem mniej zażenowany, ale z drugiej to w gruncie rzeczy tylko spóźnione o kilka(naście?) lat narzekanie o dewaluacji fotografii i lament nad powszechnym dostępem do niej. To chyba nie o to takie przesłanie w tym całym TEDzie miało chodzić?

Iwona Blecharczyk której przekaz można (serio, nic więcej sensownego nie powiedziała) skrócić do "kobiety też jeżdżą TIRami, nie jest lekko bo ludzie są uprzedzeni do kobiet za kółkiem TIRa, ale jak się uprzesz to można". W sensie, fajnie, gratuluję spełnienia marzeń, ale co mnie to.

Łukasz Kozak, który... w sumie nie wiem o czym. Na początku przypadkiem zahaczył o filter bubble, potem o tym że nawet "durne" i "tabloidowe" teksty mogą dać nam ciekawą perspektywę na historyczną kulturę. Trochę shipowania Polony [4], trochę o tym że jeż jedzący jabłka to mem pochodzący ze średniowecza. Mało spójnie, ale to nadal jedna z lepszych prelekcji tego dnia...

...w totalnym kontraście do koszmarnego bełkotu Joanny Białobrzeskiej o "Fast Life". Byłem autentycznie poirytowany że ktoś jej pozwolił wejść na scenę opowiadając to co opowiadała. Zaczynając od zupełnie do mnie niezrozumiałej krytyki "Przedszkoli 24H" (w ogóle, wiedzieliście że coś takiego istnieje? Płacisz małą furgonetkę hajsu (~250zł) i oddajesz dzieciaka na 24h. Wyobrażam sobie że to super sprawa w jakichś kryzysowych sytuacjach, albo po prostu jak masz ochotę na krótki urlop od dziecka) i lament o tym że dzieci zostają w nich NAWET DO SZEŚCIU DNI, poprzez narzekanie że w naszej kulturze śmierć jest nieobecna (ktoś powinien jej przypomnieć że żyjemy w cywilizacji śmierci), aż do wrzucenia obligatoryjnego "a gry wideo są złe bo tam się uczy zabijać". Do tego jeszcze trochę narzekania na to jaki to system edukacji jest zły i okropny i dzieci są tłamszone i w ogóle. Wszystko to w koszmarnej analogii do tego że współczesne życie jest "Fast Life", tak jak "Fast Food" - niby zaspokaja głód, ale na dłuższą metę jest niezdrowe. Nie guglajcie jej, bo jak zobaczycie wywiady z nią to się już zupełnie idzie pochlastać (słowa kluczowe: gender, patriotyzm, tożsamość narodowa, rzyg). 

Po tym z kolei był Paweł Janas, z najciekawszym wykorzystaniem akordeonu jaki w życiu słyszałem.

##Czwarta sesja

W ostatniej sesji nie było nic wartego uwagi.

Michał Mikulski z robotem mającym wspierać rehabilitację. Sorry, nie kupuję tego, w medycynie jest już i tak zdecydowanie za dużo woo, to nie jest dziedzina gdzie wystąpienie na konferencji powinno o czymkolwiek świadczyć, badania kliniczne potwierdzające skuteczność tego robota albo idźcie do domu.

Potem dowiedziałem się o tym że praca weterynarza nie jest łatwa i że ich usługi są drogie bo koszty naszego leczenia pokrywa przecież ZUS a zwierzęta nie są ubezpieczone. such insight, many enlightment, wow (przepraszam, ale ja już serio nie mogę.) 

Dwójka duńskich studentów medycyny, którzy nie tylko są studentami medycyny, ale też przyjaciółmi i artystami i w ogóle robią murale. Znowu, super, ale co mnie to w ogóle.

Na koniec Erica Hargreave z jakimś pseudotetralnym przedstawieniem o niczym. 

---
Po tych 10 godzinach nie czułem się zainspirowany. Nie chciałem wyjść ze swojej strefy komfortu. Poczułem się wytrollowany z 10h mojego życia i zażenowany entuzjastycznymi reakcjami gawiedzi na to co padało ze sceny. (jak np. ta perełka: [5], albo ta: [6]) 

Jak więc możecie się domyślać, Janek nie polubił TEDxWarsaw i nadal jest nastawiony sceptycznie.

[1]:http://www.bratton.info/projects/talks/we-need-to-talk-about-ted/
[2]:http://blog.ted.com/2013/12/16/the-most-popular-20-ted-talks-2013/
[3]:http://blog.okcupid.com
[4]:http://polona.pl
[5]:https://twitter.com/friedtulip/status/444159967546048513
[6]:https://twitter.com/sle3va/status/444079250057228289