---
title: Windows Phone / Endomondo rant
layout: post
header: Windows Phone / Endomondo rant
---
No więc Windows Phone (albo developerzy aplikacji na niego, ale jako endusera bardzo mi wszystko jedno kto jest za to odpowiedzialny) zarobił sobie kolejną żółtą kartkę u mnie, do tego stopnia że zupełnie porzuciłem jakiekolwiek plany nabycia słuchawki z tym systemem w najbliższym czasie.

No więc wyobraźcie sobie taką sytuację.
Jesteście grubi (część z was nie musi sobie wyobrażać, macie łatwiej!). No ale postanawiacie w pewnym momencie swojego życia coś z tym zrobić. No więc sznurujecie buty na nogach, zakładacie spodnie z papieżem i bierzecie telefon z zainstalowanym endomondo (na WP nie ma runkeepera, wybaczcie.). 
Schodząc po schodach klikacie w ikonkę Spotify i wybieracie którąś z waszych ulubionych playlist. Jesteście już na dole, także odpalacie endomondo, klikacie początek treningu i zaczynacie mknąć przed siebie (tak naprawdę to podczas biegu macie grację podobną do słonia spierdalającego przed myszą na dwóch nogach, ale pomińmy ten fragment.). 

Cytując znanego i lubianego filozofa Łonę: "I tu zaczynają się jaja!":
Mianowicie po rozpoczęciu treningu, endomondo rzuca jakimś debilnym tekstem w stylu "Give me your best!" albo "Free your endorphins!" czy podobnym sucharem. Różnice kulturowe, etc., może komuś za wielką wodą wydał się to być spoko pomysł, nie wnikam. Jednakże po 30 sekundach od tego komunikatu zauważacie że coś jest nie tak. 
Muzyka przestała grać. Ale będąc przykładnymi polakami, jedyne co robicie to klniecie pod nosem (albo, jeżeli jesteście mną, to macie pewne problemy z dostosowaniem głośności wydawanych odgłosów do otoczenia i cały przystanek autobusowy pełny ludzi ogląda się za tobą, zastanawiając dlaczego ktoś tak siarczyście klnie.), minimalizujecie endomondo i znowu wybieracie playlistę w spotify. 
Teraz już podczas biegu, więc nie chce wam się skrollować na sam dół, więc wybieracie jedną z pierwszych, nie tę ulubioną, co jedynie podwyższa wasze wkurwienie.

No ale w końcu muzyka zaczyna znowu grać, wy poruszacie się z prędkością niewiele większą niż przejechany jeż, pocicie się jak grube świnie, ale biegniecie. 

Napisałbym że biegniecie, i biegniecie, i biegniecie, ale pamiętajcie - jesteście grubi, także biegniecie te niecałe marne dwa kilometry i jesteście już pod domem. 

Nawet nieco zadowoleni z siebie, bo zaledwie cztery dni wcześniej po połowie tej trasy trzeba was było zeskrobywać z asfaltu, a teraz udało się przebiec nawet całą - dumnie odblokowujecie telefon z zamiarem zakończenie workoutu. Klikacie na ikonkę endomondo. 

W tym momencie macie ochotę (mini quiz!):

* a) kogoś zabić
* b) rozjebać telefon na drobne kawałeczki
* c) pojechać do Redmond i wyjaśnić sobie kilka spraw z ludźmi od Windows Phone
* d) zabronić developerom endomondo dotykania klawiatury kiedykolwiek więcej w życiu
* e) zabić się
* f) wszystkie powyższe po trochu

Prawidłowa odpowiedź, to oczywiście odpowiedź f). Dlaczego? 

Otóż, moi drodzy czytelnicy, aplikacja endomondo radośnie powitała was komunikatem że was workout został spauzowany przy 0.01 km (czyli wtedy kiedy wkurwieni kliknęliście drugi raz play w spotify), i czy może chciałbyś go wznowić?

No więc, kurwa, nie, nie chciałbym go, kurwa wznowić, bo właśnie go kurwa skończyłem, ty pierdolona kupo zerojedynkowego gówna.