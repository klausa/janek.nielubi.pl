---
title: 'Batman: Arkam City'
layout: post
header: 'Dlaczego nie lubię Batmana: Arkham City'
---
Znowu narzekam na gry wideo! Tym razem padło na Batmana: Arkham City. [1]

Na początek, CO DO KURWY NĘDZY jest nie tak z głosem batmana!? Najgorszy voice-acting jaki w życiu słyszałem. [2]

Brzmiało jakby nagrywali go kiedy ćwiczył swoje kwestie na kiblu. Zupełnie suche i wymuszone. [3]

« tutaj wstaw moje zwyczajowe narzekania na poziom trudności wzrastający w zupełnie złych miejscach » [4]

Tak, tak, rozumiem, chesz żeby im dalej w grę, tym było coraz trudniej, ale niech najtrudnieszje miejsca będą w okolicach walk z bossami, a nie w kompletnie losowych miejscach. Proszę. [5]

Losowe miejsca będące niewytłumaczalnie trudne powodują tylko to, że mam ochotę jebnąć padem o ścianę i nigdy więcej nie odpalić twojej gry. [6]

Fabuła: nigdy nie wiedziałem że coś ważnego zaraz się stanie, dopóki się już nie stało. [7]

Ale może to tylko ja, nie wiem, w każdym razie nie czułem żadnego rosnącego napięcia przed ważnymi momentami. [8]

Sama walka była dobra (nie świetna, ale dobra), spory wybór gadżetów był miłym dodatkiem, z kolei walki z bossami były wszędzie od 'O KURWA CO TO TO ZA GÓWNO' do 'hej, to było naprawdę niezłe!' (Freeze był świetny.) [9]

Ta-dziwna-kraina-do-której-wchodzisz-z-Ra's-al-Ghulem-czy-jakkurwakolwiek-pisze-się-jego-imię była świetna i… świeża. [10]

No i sama gra miała jak na mnie, prawie idealną długość. Ogólnie - 7.5/10, polecam. [11]



[1]: https://twitter.com/klausa_qwpx/status/277835134147911680
[2]: https://twitter.com/klausa_qwpx/status/277835379313356802
[3]: https://twitter.com/klausa_qwpx/status/277835581302665217
[4]: https://twitter.com/klausa_qwpx/status/277835834223390720
[5]: https://twitter.com/klausa_qwpx/status/277835977781805056
[6]: https://twitter.com/klausa_qwpx/status/277836088385609731
[7]: https://twitter.com/klausa_qwpx/status/277836527768313856
[8]: https://twitter.com/klausa_qwpx/status/277836723948498944
[9]: https://twitter.com/klausa_qwpx/status/277836983361994753
[10]: https://twitter.com/klausa_qwpx/status/277837423285776384
[11]: https://twitter.com/klausa_qwpx/status/277837583545933824
