---
title: Nienawidzę graczy
layout: post
header: Nie lubię graczy, czyli dlaczego iOS to pełnoprawna platforma do gier.
---
Mam pewne wyznanie - nie lubię graczy. To jedna z najmniej przyjemnych społeczności
z jakimi miałem styczność w internecie - a uwierzcie mi na słowo, bywałem w różnych miejscach.
 
Nie lubię graczy, bo to grupa której niezwykle silnie wydaje się że coś jej się 'należy'.
    
Nie lubię graczy, bo to grupa jest niespotykanie wroga względem innych platform niż ich własna.
Nawet święta wojna Mac vs Linux vs Windows jest bardziej cywilizowana i nieskończenie bardziej merytoryczna niż wojenki graczy.
   
Nie lubię graczy, bo to grupa bojąca się nowego.
  
Ale przede wszystkim, nie lubię graczy, bo to grupa która akceptuje tylko najbardziej zatwardziałych reprezentantów swojego gatunku, traktując ludzi którzy po prostu chcą zagrać w prostą grę jako gorszych. Grupa, której się wydaje że 'casual' to pejoratywne określenie. 

Zupełnie nie rozumiem wielkiego oburzenia i powszechnego obruszenia wobec *kolejnego* day-one DLC, albo DLC znajdującego się już na płycie. 
Kupujecie filmy na DVD/Blu-Ray, prawda? Często mają one 'usunięte sceny', czy nawet 'specjalne reżyserskie edycje' samego filmu - jednak jakoś nigdy, **nigdy** nie słyszałem kinomanów narzekających na to że muszą zapłacić żeby zobaczyć te sceny. Przecież były one już gotowe, nakręcone, czemu nie mogły być zamieszczone w filmie który widziałem w kinie?! Skandal! 

To właśnie day-one DLC. I to w ekstremalnym uproszczeniu, pomijając już takie szczegóły, jak to że między wypuszczeniem gry do tłocznia jej pojawieniem się na półkach sklepowych mija zazwyczaj dobry miesiąc - *w tym czasie też powstaje day-one DLC*. Szokujące, wiem. 
[Ciekawie na ten temat wypowiadał się Sean Random z Gearbox Software.][1]

Hipotetycznie założę że kupujecie kopie Windowsa i Office'a ze sklepu. Założę, chyba dość realistycznie, że zazwyczaj są to jedne z najtańszych wersji. Wiecie o tym, że wszystie płyty tłoczone są tak samo, i to jaka edycja zostanie zainstalowana, zależy tylko od klucza, który wpiszecie, prawda?  "Jak oni śmią żądać pieniędzy za coś, co już kupiłem i mam na płycie! To skanal, nikt tak nie robi!" Hmm. Na pewno? Nie zrozumcie mnie źle - ja też nie lubię on-disc DLC, ale nie oszukujmy się że to, co jest na płycie w jakikolwiek sposób nam się 'należy'.

Czy trzeba w ogóle tłumaczyć, dlaczego wrogość graczy wobec innych platform jest najzwyczajniej w świecie głupia? Pozwólcie, że przedstawię wam, jak wygląda różnica między Xboksem a PS3 dla jakichś 99% społeczeństwa:

1. Xbox ma wygodniejszego pada.
2. Za granie przez internet na Xboksie trzeba płacić, a na PS3 nie.
3. PS3 odtwarza filmy na Blu-Ray.
4. Na Xboksie można piracić.
5. Xbox był dość długo tańszy od PS3.

Tyle. Cała reszta, to z całym szacunkiem, pierdoły i zupełnie nieistotne szczegóły. Jasne, możesz być fanem jednego lub drugiego GoW, Halo albo Killzone, ale nie oszukujmy się - dla większości to nie jest żadna różnica, bo i tak grają w Call of Duty i GTA. Wydaje wam się że większośc ludzi widzi różnicę między Gran Turismo a Forzą? No to macie rację, wydaje wam się.
    
 Uważasz że iOS to nie jest pełnoprawna platforma to gier? Jesteś częścią problemu.  Najpopularniejszy (a przynajmniej ja najczęściej go widzę) argument przeciwko iOS to "ALE PRZECIEŻ TAM NIE MA KLAWISZY!". Owszem, nie ma. Owszem, pewien podzbiór gier przeniesiony na ekrany dotykowe, nie ma sensu bytu. Ale... co z tego? Za każdym razem, gdy wychodzi nowa konsola przenośna słychać płacz i narzekanie, że nie ma na nią żadnych ciekawych gier, tylko porty z "dużych" konsol. Przy każdym kolejnym Call of Duty, wszyscy narzekają że Activision odcina kupony, nie robiąc nic nowego. Kiedy pojawia się platforma która *ZMUSZA* deweloperów do wymyślania nowych rzeczy, gracze kręcą nosem. Nie rozumiem. Po prostu nie rozumiem. Przypomina mi to trochę neckbeardów na początku lat osiemdziesiątych, narzekających że 'te komputery z grafiką gdzie się klika to się nigdy nie przyjmą, to dla dzieci jest, wszyscy będą ciągle używać konsoli i pisać swoje programy w Basicu'. Cóż, historia udowodniła kto ma rację. 
    
 Ale największym obrzydzeniem napawają mnie ludzie, którzy patrzą z góry na 'casuali' i 'casualowe gry'. Jak ślepym trzeba być, żeby udawać że to jest gorsza platforma? Weźmy Angry Birds. Jedna gra. 700 milionów pobrań. Więcej niż poprzednie trzy generacje "dużych" konsol razem wzięte. 18 milionów sprzedanych Kinectów. Około 10 milionów PS Move. Apple w ciągu **jednego kwartału** sprzedaje 40 milionów iPhone'ów (a nie wliczamy w to iPadów ani iPodów touch, o urządzeniach korzystających z Androida nawet nie wspominając). Około 60% właścicieli smartfonów używa ich do grania. Ogromne ilości. Co na to gracze? "Eeee tam, popierdółki dla casuali.". 
 
 Krew mi się gotuje. Skoro nie ludzie głosujący swoimi portfelami decydują, co jest "prawdziwą platformą do gier", to nie wiem co decyduje. Czy jeżeli Apple wydałoby przystawkę do iPada, które dodawałoby przyciski ([Kamil pisał o takim projekcie kilka dni temu][2]), to wtedy gracze byliby zadowoleni? Dlaczego w takim razie Xperia Play była taką katastrofalną porażką? "Nie ma na to prawdziwych gier!" - serio? Twórcom Shadowgun, Inifnity Blade, Minecrafta, ludziom którzy portowali GTA III, którzy portują Baldur's Gate i wielu, wielu innym byłoby przykro. To, że nie ma **twojej** ulubionej gry, lub jej odpowiednika, nie znaczy że nie ma "prawdziwych" gier. Co to w ogóle znaczy, "prawdziwa gra"?! 
Jakoś nie słychać narzekań kinomanów na to, że wszyscy czekają na nowego "Mrocznego Rycerza", zamiast interesować się fajnymi, ale bardziej niszowymi produkcjami. Ludzie, którzy narzekają że muzyka, powiedzmy, Lady Gagi jest płytka i nijaka, cóż, też nie mają zbytnio pojęcia o muzyce. Słuchasz vegetarian progressive grindcore'u? Świetnie, ale jeżeli uważasz że słuchający Gagi czy Rihanny słuchają "gorszej" muzyki - to jesteś zwyczajnym bucem. Lubisz Densha de Go! i gardzisz ludźmi grającymi w Angry Birds? Jesteś bucem. Ta otoczka elitarności, odbieranie prawa nazywania się "graczami" ludziom, którzy nie lubią takiej czy innej formy tej rozrywki - to wszystko, co jest złe z obecną społecznością.

Wiecie kim jest dla mnie gracz? To osoba która lubi grać w gry wideo. Tyle. Tak, moja mama która spędza niezdrowe ilości czasu przy kiepskich flashowych klonach Tetrisa i Arkanoida też jest graczem (graczką?). Twoja siostra, która z koleżankami gra w Draw Something. Brat, który naparza kombosy w Tekkenie z prędkością światła. Tata, który czerpie **fun** ze strzelania ptakami do świń.

A że nie pogadam z nimi o moich ulubionych tytułach, o co-opie nie wspominając? No cóż, o moim ulubionym filmie z Sashą Grey też z nimi nie pogadam.

 
[1]: http://kotaku.com/5898480/day-one-dlc-isnt-always-evil-says-borderlands-2-guy
[2]: http://niezgrani.pl/2012/04/04/pad-apple-ios-ipad-ipod-appstore/
