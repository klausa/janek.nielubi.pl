---
title: Ikea.
layout: post
header: Ikea.
---
Siedzę z Karolina w Ikei i dyskretnie (tzn. cały czas się na nich gapimy) obserwujemy najbardziej obrzydliwą parę na świecie. 

Co jest z nimi nie tak, zapytacie?

Zacznijmy może od tego ze są w Ikei z psem. Tzn, psem wielkości chomika, ale niby ciągle psem. 

Ale to jest najmniejszy z problemów. Otóż właścicielka tego psa jest w nim najwyraźniej strasznie zakochana. Skąd to wiemy? Wzięła swojego pupilka na ucztę do sklepowej restauracji. 

Ale to nie jest normalny pies, ani to nie jest normalna uczta. Psy tej wielkości są najwyraźniej bezużyteczne do tego stopnia, ze nie potrafią nawet gryźć swojego jedzenia. 
Ale od czego taki pies ma swoją ukochaną paniusię!
Ona z dzika chęcią napakuje sobie ryj klopsikami, chwile pożuje, wypluje na dłoń i nakarmi to małe, włochate, śmierdzące, bezuzyteczne gówno.

Jakby tego było mało, to piesek miał najwyraźniej rownież ochotę na zupę. Wydawałoby sie ze tutaj sprawa prostsza, nie trzeba nic żuć, można po prostu dać żreć z łyżki. Haha, nie. Wszyscy przecież wiedza ze odpowiednim protokolem jest wylanie sobie zupy na dłoń i danie psu żeby ja wylizał. 

A mi czasami wydaje sie ze to ja jestem popierdolony.