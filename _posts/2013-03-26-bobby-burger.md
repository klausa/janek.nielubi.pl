---
title: Bobby Burger
layout: post
header: Bobby Burger
---
Dobra, recenzja kolejnej burgerowni.

Byliśmy wczoraj z [Pawłem][1] i [Kamilem][2] w "nowym" [Bobby Burger][3]. 
W sumie nie wiem czy nowym, czy kolejnym, pogubiłem się.

Kiedyś, to był obwoźny bus który zatrzymywał się prawie-że-pod-naszymi oknami i był całkiem zajebisty. Potem weszli w jakąś dziwną kolaborację z Warszawa Powiśle, teraz otworzyli się na Żurawiej.

No i lokal na Żurawiej jest genialny. Po pierwsze - *W KOŃCU* burgerownia która nie jest wielkości znaczka pocztowego. Jest gdzie usiąść, przy stole da się nawet nieco wyciągnąć nogi, wystrój prosty, ale ma swój urok. W tle gra fajna muzyka (dożywotnie propsy za Łonę, zawsze i wszędzie!), generalnie cud mjut i orzeszki.

No ale przejdźmy do tego co najważniejsze, czyli do jedzenia.
Pojawiliśmy się tam w sumie dlatego, że wypatrzyłem na fejsie że mają ciekawie podane chilli con carne [4] i od tego też zaczęła się nasza przygoda.

No więc nie oszukujmy się - albo ktoś ostatnio przedefiniował chilli con carne, albo to nie było chilli. Najbliżej czemukolwiek co potrafiłbym nazwać, było to sosowi do spaghetti bolognese, ale nie przejmujmy się takimi szczegółami. 

Najważniejsze jest to, że smakuje absolutnie genialnie. Połączenie pomidorowego (nawet lekko pikantnego) sosu zapieczonego serem z frytkami jest po prostu mistrzowskie - oprócz tego że Gruby i Kamil wpierdolili większość mojej porcji, nie miałbym nic do zarzucenia.

No ale najważniejsze w Bobbym są rzecz jasna burgery. Nie oszukujmy się - z niejednego grilla jedliśmy i trudno zaprzeć nam dech w piersiach, więc nikt z nas nie spodziewał się zniszczenia naszych mózgów. 
Pomimo kilku uwag - to był jeden z najlepszych burgerów które w życiu jadłem. 

Mięso - mogłoby być odrobinę krócej na grillu, ale nie było też dramatu. 
Nieco rozlatująca się bułka to taka oczywistość i standard przy burgerach, że już nie mam nawet serca odejmować za to punktów. Zwróciłem natomiast uwagę na to że mój burger był nieco 'cieknący' - pomijając już nieco zbyt 'lejącą' na mój gust konsystencję sosu, miałem wrażenie jakby ktoś niewystarczająco odsączył sałatę czy pomidory.

Jedne z najlepszych burgerów w tym mieście, genialny starter w postaci "chilli" (double air quotes), kawa w opcji z dolewką, fajny klimat, w miarę normalne ceny, lokalizacja w centrum miasta.

9/10, Znak Jakości Janusza, polecam i jak będę miał okazję zjeść zachwalane przez obsługę pancake'i też zrelacjonuję.

[1]: https://www.facebook.com/pawelorzech
[2]: https://www.facebook.com/kamil.szmyd
[3]: https://www.facebook.com/burgerbobby
[4]: https://www.facebook.com/photo.php?fbid=557868404246699&set=a.557401250960081.1073741827.424376747595866&type=1
